//
//  ImageViewCell.h
//  CollectionViewTutorial
//
//  Created by Admin on 13/04/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewCell : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UILabel *myImageLabel;
@property (nonatomic,weak) IBOutlet UIImageView *myImage;

@end
