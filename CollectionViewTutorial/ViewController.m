//
//  ViewController.m
//  CollectionViewTutorial
//
//  Created by Admin on 13/04/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myImageArray = @[@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck",@"donaldDuck"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CollectionView Methods
//Kaç bölümden oluşacagını belirtir.
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

//Bolumde kac eleman olacagını belirtir.
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.myImageArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    ImageViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageViewCell" forIndexPath:indexPath];
    NSString *imageStrings = [self.myImageArray objectAtIndex:indexPath.row];
    cell.myImage.image = [UIImage imageNamed:imageStrings];
    cell.myImageLabel.text = imageStrings;
    return cell;
}

//Her bir hucrenin genişlik ve yuksekligini belirtir.
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake(150.0, 150.0);
}

//Hucre arası ve ekran uzerindeki boslukları belirtir.
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"test_brach LOG");
    NSLog(@"test_brach LOG2");
    NSLog(@"selected collection view item %ld",(long)indexPath.row);
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UITabBarController *tabbarController = [storyboard instantiateViewControllerWithIdentifier:@"tabbarController"];
    [self.navigationController pushViewController:tabbarController animated:YES];
}

@end
