//
//  ViewController.h
//  CollectionViewTutorial
//
//  Created by Admin on 13/04/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ImageViewCell.h"

@interface ViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(retain, nonatomic) IBOutlet UICollectionView *myCollection;
@property(strong, nonatomic) NSArray *myImageArray;

@end

