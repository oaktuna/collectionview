//
//  AppDelegate.h
//  CollectionViewTutorial
//
//  Created by Admin on 13/04/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

